====================================================================================
Latex Source for Book of Abstracts and Programme of Airborne Wind Energy Conferences
====================================================================================

Author:      Roland Schmehl
             Delft University of Technology
             Tel.: +31 15 278 5318
             Mob.: +31 61 495 6025
             Email r.schmehl@tudelft.nl
         
Conferences: www.awec2015.eu

Status
======
+----+--------------+-----+-----+-----+-----+---------------------------------------
       First Author  Photo Addr  Logo  Text  Remarks
+----+--------------+-----+-----+-----+-----+---------------------------------------
AWEC0A Schmehl       OK   OK    OK    OK      Welcome note
AWEC01 Schmehl       OK   OK    OK    OK      First abstract
+----+--------------+-----+-----+-----+-----+---------------------------------------

Structure
=========
Each abstract is located in a directory AWECxx where xx is starting at 01.
The directory AWEC00 contains a template chapter that is not used in the final book.
Front matter, such as welcome notes are in directories AWECyy where yy starts at 0A.

Requirements
============
CTAN texlive 2014

Compile the book of abstracts
=============================
Use pdflatex on file book.tex and makeindex on the generated file book.idx to generate 
the pdf of the book of abstracts (file book.pdf). For simplicity this book of abstracts 
uses a hand-coded short bibliography per abstract rather than a central bib file.

Compile the conference programme
================================
Use pdflatex on file programme.tex to generate the conference programme (file 
programme.pdf).

   

